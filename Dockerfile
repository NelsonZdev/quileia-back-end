FROM maven:3-openjdk-11 as target
WORKDIR /build
COPY pom.xml .
RUN mvn dependency:go-offline

COPY src/ /build/src
RUN mvn package -DskipTests

FROM openjdk:11-jre as production-stage
EXPOSE 8443
COPY --from=target /build/target/PruebaQuileia.jar /
CMD ["java", "-jar", "/PruebaQuileia.jar"]


package com.example.quileia.service;


import com.example.quileia.entity.Agent;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.exception.TrackAssignationException;


public interface AgentService {
	
	public Iterable<Agent> getAllAgents();

	/**
	 * Permite crear un agente en la base de datos
	 * 
	 * @param newAgent Contiene toda la información del agente que se desea añadir, incluida la vía.
	 * @return Una vez se ha creado el agente en base de datos, se retorna con la información del agente.
	 */
	public Agent createAgent(Agent newAgent);
	
	/**
	 * Muestra la información de un agente especifico en la base de datos.
	 * @param id Id del agente que se necesita información.
	 * @return Si el agente se encuentra en la base de datos se retorna su información.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 */
	public Agent getAgentById(int id) throws NotFoundException;
	
	/**
	 * Permite actualizar la información de una agente en la bse de datos.
	 * @param agent Contiene la información del agente que sesea ser editado, incluida la vía.
	 * @return Una vez se ha editado el agente en base de datos, se retorna con la información actualizada.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 * @throws TrackAssignationException Se lanza esta excepción en caso de que se intente asignar más de 
	 * 									tres veces una vía al agente.
	 */
	public Agent updateAgent(Agent agent) throws NotFoundException, TrackAssignationException;
	
	/**
	 * Permite eliminar un gente de la base de datos.
	 * @param id Id del agente del que se desea eliminar en la base de datos.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 */
	public void deleteAgent(int id) throws NotFoundException;

}

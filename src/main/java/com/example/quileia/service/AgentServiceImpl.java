package com.example.quileia.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.exception.TrackAssignationException;
import com.example.quileia.repository.AgentRepository;



@Service
public class AgentServiceImpl implements AgentService {
	
	@Autowired
	AgentRepository agentRepository;
	
	@Autowired
	HistoryService historyService;

	@Override
	public Iterable<Agent> getAllAgents() {
		return agentRepository.findAll();
	}
	
	@Override
	public Agent createAgent(Agent newAgent) {
		Agent dbAgent = agentRepository.save(newAgent);
		HistoryAssignation history = new HistoryAssignation(0,dbAgent,dbAgent.getCurrentTrack(),LocalDateTime.now(),null,true);
		historyService.createHistory(history);
		return dbAgent;
	}

	@Override
	public Agent getAgentById(int id) throws NotFoundException {
		return agentRepository.findById(id).orElseThrow(()-> new NotFoundException("Agent not exist"));
	}

	@Override
	public Agent updateAgent(Agent agent) throws NotFoundException, TrackAssignationException {
		Agent dbAgent = getAgentById(agent.getId());
		
		Track currentTrack = dbAgent.getCurrentTrack();
		
		if(agent.getCurrentTrack().getId() != dbAgent.getCurrentTrack().getId()) {
			
			if(historyService.countAssignationsToTrack(dbAgent, agent.getCurrentTrack()) < 3) {
				
				HistoryAssignation dbHistory = historyService.getHistoryByAgentAndTrack(dbAgent, dbAgent.getCurrentTrack(), true);
				historyService.updateHistory(dbHistory);
				
				HistoryAssignation history = new HistoryAssignation(0,dbAgent,agent.getCurrentTrack(),LocalDateTime.now(),null,true);
				historyService.createHistory(history);
				
				currentTrack = agent.getCurrentTrack();
				
			}else {
				agent.setCurrentTrack(currentTrack);
				mapAgent(dbAgent, agent);
				agentRepository.save(dbAgent);
				throw new TrackAssignationException("Track no update more 3 routes assig");
			}
			
		}
		agent.setCurrentTrack(currentTrack);
		mapAgent(dbAgent, agent);
		return agentRepository.save(dbAgent);
	}
	
	private void mapAgent(Agent dbAgent, Agent newAgent) {
		dbAgent.setName(newAgent.getName());
		dbAgent.setLastName(newAgent.getLastName());
		dbAgent.setYearsExperience(newAgent.getYearsExperience());
		dbAgent.setSecretary(newAgent.getSecretary());
		dbAgent.setCurrentTrack(newAgent.getCurrentTrack());
	}

	@Override
	public void deleteAgent(int id) throws NotFoundException {
		Agent agent = agentRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Agent not Found in deleteUser -"+this.getClass().getName()));
		historyService.deleteHistoryOfAgent(agent);

		agentRepository.delete(agent);
	}

	
}

package com.example.quileia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.repository.TrackRepository;

@Service
public class TrackServiceImpl implements TrackService {

	@Autowired
	TrackRepository trackRepository;
	
	@Override
	public Iterable<Track> getAllTracks() {
		return trackRepository.findAll();
	}

	@Override
	public Track createTrack(Track track) {
		return trackRepository.save(track);
	}

	@Override
	public Track getTrackById(int id) throws NotFoundException {
		 
		return trackRepository.findById(id).orElseThrow(()-> new NotFoundException("Track not exist"));
	}

	@Override
	public Track updateTrack(Track track) throws NotFoundException{
		Track dbTrack = getTrackById(track.getId());
		mapTrack(dbTrack, track);
		return trackRepository.save(dbTrack);
	}
	
	private void mapTrack(Track dbTrack, Track newTrack) {
		dbTrack.setNumber(newTrack.getNumber());
		dbTrack.setStreetOrCarrer(newTrack.getStreetOrCarrer());
		dbTrack.setType(newTrack.getType());
		dbTrack.setLevelCongestion(newTrack.getLevelCongestion());
	}

	@Override
	public void deleteTrack(int id) throws NotFoundException {
		Track dbTrack = getTrackById(id);
		trackRepository.delete(dbTrack);
	}

	@Override
	public Iterable<Track> getAllTrackslevelCongestionGreaterThan(double levelCongestion) {
		return trackRepository.findAllBylevelCongestionGreaterThan(levelCongestion);
	}

}

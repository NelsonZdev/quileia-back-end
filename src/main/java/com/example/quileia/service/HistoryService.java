package com.example.quileia.service;

import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;


public interface HistoryService {
	
	/**
	 * Muestra todos los datos de la tabla history_assignation de la base de datos
	 * @return Retorna un arreglo que contiene todo el historial que se encuantra en la base de datos. En caso de no encontrar historias retorna un arreglo vacío 
	 */
	public Iterable<HistoryAssignation> getAllHistory();
	
	/**
	 * Permite crear una historia en la base de datos.
	 * @param history Contiene la información de la historia que se agregara a la base de datos.
	 * @return Una vez se ha creado la historia en la base de datos, se retorna con la información de esta. 
	 */
	public HistoryAssignation createHistory(HistoryAssignation history);
	
	/**
	 * Muestra el número de asignaciones que ha tenido un agente a una vía.
	 * @param agent Contiene la información del agente que se desea saber el número de asignacniones
	 * @param track Contiene la información del la vía que se desea evaluar
	 * @return Retorna el número de asignaciones que el agente ha sido asignado a la vía.
	 */
	public long countAssignationsToTrack(Agent agent, Track track);
	
	/**
	 * Pemite actualizar la información de una historia en la base de datos.
	 * @param history Contiene la información de la historia que sera actualizada.
	 * @return Una vez se ha editado la historia en la base de datos, se retorna con la información actualizada. 
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la historia en la base de datos.
	 */
	public HistoryAssignation updateHistory(HistoryAssignation history) throws NotFoundException;

	/**
	 * Muestra el historial filtaro por agente, vía y estado(Activo o Inactivo).
	 * Este metodo esta pensado para traer la historia de la vía actual de un agente.
	 * @param agent Contiene la información del agente que servira como filtro para la busqueda.
	 * @param track Contrene la información de la vía que servira como filtro para la busqueda.
	 * @param active Estado que servira como filtro para la busqueda.
	 * @return Retorna la historia filtrada por agente, vía y estado, se busca que se la historia de la ví actual.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la historia en la base de datos.
	 */
	public HistoryAssignation getHistoryByAgentAndTrack(Agent agent, Track track, boolean active) throws NotFoundException;

	/**
	 * Muestra una historia especifica en la base de datos.
	 * @param id Id de la historia que se necesita información.
	 * @return Retorna la historia asocida a la Id enviada.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la historia en la base de datos.
	 */
	public HistoryAssignation getHistoryById(int id) throws NotFoundException;
	
	/**
	 * Eliminar el historial completo de una agente en la base de datos.
	 * @param agent Contiene los datos del agente al cual se le desea eliminr el historial.
	 */
	public void deleteHistoryOfAgent(Agent agent);
	
	/**
	 * Muestra el historial de un agente especifico.
	 * @param agent Contiene la informaión del agente del que se desea traer el historial.
	 * @return Retorna la un arreglo con las historias filtrada por agente.
	 */
	public Iterable<HistoryAssignation>getHistoryByAgent(Agent agent);
	
	/**
	 * Muestra el historial de una vía especifica.
	 * @param track Contiene la informaión del la vía que se desea traer el historial.
	 * @return Retorna la un arreglo con las historias filtrada por vía.
	 */
	public Iterable<HistoryAssignation>getHistoryByTrack(Track track);

}

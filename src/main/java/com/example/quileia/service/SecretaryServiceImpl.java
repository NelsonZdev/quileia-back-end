package com.example.quileia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.quileia.entity.Secretary;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.repository.SecretaryRepository;

@Service
public class SecretaryServiceImpl implements SecretaryService {

	@Autowired
	SecretaryRepository secretaryRepository;
	
	@Override
	public Iterable<Secretary> getAllSecretarys() {
		return secretaryRepository.findAll();
	}

	@Override
	public Secretary getSecretarysById(int secretaryId) throws NotFoundException {
		return secretaryRepository.findById(secretaryId).orElseThrow(() -> new NotFoundException("Secretary Dont Exixt"));
	}

	@Override
	public Secretary createSecretary(Secretary secretary) {
		return secretaryRepository.save(secretary);
	}

	@Override
	public Secretary updateSecretary(Secretary secretary) throws NotFoundException {
		Secretary dbSecretary = secretaryRepository.findById(secretary.getId()).orElseThrow(()-> new NotFoundException("Secretary not exist"));
		mapSecretary(dbSecretary,secretary);
		return secretaryRepository.save(dbSecretary);
	}
	
	private void mapSecretary(Secretary dbSecretary, Secretary newSecretary) {
		dbSecretary.setName(newSecretary.getName());
	}

	@Override
	public void deleteSecretary(int id) throws NotFoundException {
		Secretary dbSecretary = getSecretarysById(id);
		secretaryRepository.delete(dbSecretary);
	}

}

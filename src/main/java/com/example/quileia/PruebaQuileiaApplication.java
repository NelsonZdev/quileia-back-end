package com.example.quileia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaQuileiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaQuileiaApplication.class, args);
	}

}

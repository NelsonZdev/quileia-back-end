package com.example.quileia.dto;

import com.example.quileia.entity.Track;

public class TrackDTO {
	
	private int id;
	
	private String typeT;
	
	private String streetOrCarrerT;
	
	private int numberT;
	
	private double levelCongestionT;

	public TrackDTO() {
		super();
	}


	public TrackDTO(int id, String typeT, String streetOrCarrerT, int numberT, double levelCongestionT) {
		super();
		this.id = id;
		this.typeT = typeT;
		this.streetOrCarrerT = streetOrCarrerT;
		this.numberT = numberT;
		this.levelCongestionT = levelCongestionT;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTypeT() {
		return typeT;
	}


	public void setTypeT(String typeT) {
		this.typeT = typeT;
	}


	public String getStreetOrCarrerT() {
		return streetOrCarrerT;
	}


	public void setStreetOrCarrerT(String streetOrCarrerT) {
		this.streetOrCarrerT = streetOrCarrerT;
	}


	public int getNumberT() {
		return numberT;
	}


	public void setNumberT(int numberT) {
		this.numberT = numberT;
	}


	public double getLevelCongestionT() {
		return levelCongestionT;
	}


	public void setLevelCongestionT(double levelCongestionT) {
		this.levelCongestionT = levelCongestionT;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(levelCongestionT);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + numberT;
		result = prime * result + ((streetOrCarrerT == null) ? 0 : streetOrCarrerT.hashCode());
		result = prime * result + ((typeT == null) ? 0 : typeT.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrackDTO other = (TrackDTO) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(levelCongestionT) != Double.doubleToLongBits(other.levelCongestionT))
			return false;
		if (numberT != other.numberT)
			return false;
		if (streetOrCarrerT == null) {
			if (other.streetOrCarrerT != null)
				return false;
		} else if (!streetOrCarrerT.equals(other.streetOrCarrerT))
					return false;
		if (typeT == null) {
			if (other.typeT != null)
				return false;
		} else if (!typeT.equals(other.typeT))
					return false;
		return true;
	}


	@Override
	public String toString() {
		return "TrackDTO [id=" + id + ", typeT=" + typeT + ", streetOrCarrerT=" + streetOrCarrerT + ", numberT="
				+ numberT + ", levelCongestionT=" + levelCongestionT + "]";
	}


	public Track toConvertTrack() {
		Track track = new Track();
		track.setId(this.id);
		track.setNumber(this.numberT);
		track.setStreetOrCarrer(this.streetOrCarrerT);
		track.setType(this.typeT);
		track.setLevelCongestion(this.levelCongestionT);
		return track;
	}

}

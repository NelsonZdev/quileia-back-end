package com.example.quileia.dto;

import java.time.LocalDateTime;


import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;

public class HistoryAssignationDTO {
	
	private int id;
	
    private int agentIdH;
    
    private String agentNameH;
 
    private int trackIdH;
    
    private String trackNameH;
 
    private LocalDateTime startDateH;
    
    private LocalDateTime finalDateH;
    
    private boolean activeH;
    
    
    public HistoryAssignationDTO() {
    	
	}
    
    public HistoryAssignationDTO(int id, int agentIdH, String agentNameH, int trackIdH, String trackNameH,
			LocalDateTime startDateH, LocalDateTime finalDateH, boolean activeH) {
		super();
		this.id = id;
		this.agentIdH = agentIdH;
		this.agentNameH = agentNameH;
		this.trackIdH = trackIdH;
		this.trackNameH = trackNameH;
		this.startDateH = startDateH;
		this.finalDateH = finalDateH;
		this.activeH = activeH;
	}
    
    public HistoryAssignationDTO(int id, int agentIdH, int trackIdH, LocalDateTime startDateH, LocalDateTime finalDateH,
			boolean activeH) {
		super();
		this.id = id;
		this.agentIdH = agentIdH;
		this.trackIdH = trackIdH;
		this.startDateH = startDateH;
		this.finalDateH = finalDateH;
		this.activeH = activeH;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAgentIdH() {
		return agentIdH;
	}

	public void setAgentIdH(int agentIdH) {
		this.agentIdH = agentIdH;
	}

	public String getAgentNameH() {
		return agentNameH;
	}

	public void setAgentNameH(String agentNameH) {
		this.agentNameH = agentNameH;
	}

	public int getTrackIdH() {
		return trackIdH;
	}

	public void setTrackIdH(int trackIdH) {
		this.trackIdH = trackIdH;
	}

	public String getTrackNameH() {
		return trackNameH;
	}

	public void setTrackNameH(String trackNameH) {
		this.trackNameH = trackNameH;
	}

	public LocalDateTime getStartDateH() {
		return startDateH;
	}

	public void setStartDateH(LocalDateTime startDateH) {
		this.startDateH = startDateH;
	}

	public LocalDateTime getFinalDateH() {
		return finalDateH;
	}

	public void setFinalDateH(LocalDateTime finalDateH) {
		this.finalDateH = finalDateH;
	}

	public boolean isActiveH() {
		return activeH;
	}

	public void setActiveH(boolean activeH) {
		this.activeH = activeH;
	}
	

	@Override
	public String toString() {
		return "HistoryAssignationDTO [id=" + id + ", agentIdH=" + agentIdH + ", agentNameH=" + agentNameH
				+ ", trackIdH=" + trackIdH + ", trackNameH=" + trackNameH + ", startDateH=" + startDateH
				+ ", finalDateH=" + finalDateH + ", activeH=" + activeH + "]";
	}

	public HistoryAssignation toConvertHistory() {
		HistoryAssignation history = new HistoryAssignation();
		history.setId(id);
		history.setAgent(new Agent(agentIdH));
		history.setTrack(new Track(trackIdH));
		history.setStartDate(startDateH);
		history.setFinalDate(finalDateH);
		history.setActive(activeH);
		return history;
	}

}

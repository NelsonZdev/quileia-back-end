package com.example.quileia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.example.quileia.dto.SecretaryDTO;

@Entity
public class Secretary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5459353625695390511L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="native")
	@GenericGenerator(name="native",strategy="native")
	private int id;

	@Column
	private String name;
	

	public Secretary() {
		super();
	}


	public Secretary(int id) {
		super();
		this.id = id;
	}


	public Secretary(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Secretary other = (Secretary) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
					return false;
		return true;
	}


	@Override
	public String toString() {
		return "Secretary [id=" + id + ", name=" + name + "]";
	}

	public SecretaryDTO toConvertSecretaryDTO() {
		SecretaryDTO secretary = new SecretaryDTO();
		secretary.setId(id);
		secretary.setNameS(name);
		return secretary;
	}

}

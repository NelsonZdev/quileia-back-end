package com.example.quileia.entity;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import com.example.quileia.dto.HistoryAssignationDTO;


@Entity
public class HistoryAssignation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4729122582313607670L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="native")
	@GenericGenerator(name="native",strategy="native")
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "agent_id")
    private Agent agent;
 
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "track_id")
    private Track track;
 
    @Column
    private LocalDateTime startDate;
    
    @Column
    private LocalDateTime finalDate;
    
    @Column
    private boolean active;

	public HistoryAssignation() {
		super();
	}

	public HistoryAssignation(int id, Agent agent, Track track, LocalDateTime startDate, LocalDateTime finalDate,
			boolean active) {
		super();
		this.id = id;
		this.agent = agent;
		this.track = track;
		this.startDate = startDate;
		this.finalDate = finalDate;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(LocalDateTime finalDate) {
		this.finalDate = finalDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((agent == null) ? 0 : agent.hashCode());
		result = prime * result + ((finalDate == null) ? 0 : finalDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((track == null) ? 0 : track.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryAssignation other = (HistoryAssignation) obj;
		return Objects.equals(id, other.id) &&
				Objects.equals(agent, other.agent) &&
				Objects.equals(track, other.track) &&
				Objects.equals(startDate, other.startDate) &&
				Objects.equals(finalDate, other.finalDate) &&
				Objects.equals(active, other.active);
	}

	@Override
	public String toString() {
		return "HistoryAssignation [id=" + id + ", agent=" + agent + ", track=" + track + ", startDate=" + startDate
				+ ", finalDate=" + finalDate + ", active=" + active + "]";
	}
	
	public HistoryAssignationDTO toConvertHistoryDTO() {
		HistoryAssignationDTO history =new HistoryAssignationDTO();
		history.setId(id);
		history.setAgentIdH(agent.getId());
		history.setAgentNameH(agent.getName() + " " + agent.getLastName());
		history.setTrackIdH(track.getId());
		history.setTrackNameH(track.getType() + " " + track.getStreetOrCarrer() + " " + track.getNumber());
		history.setStartDateH(startDate);
		history.setFinalDateH(finalDate);
		history.setActiveH(active);
		return history;
	}


}

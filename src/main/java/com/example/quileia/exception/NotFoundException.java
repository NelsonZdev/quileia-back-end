package com.example.quileia.exception;

public class NotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7344854413939453696L;
	
	public NotFoundException() {
		super();
	}

	public NotFoundException(String message) {
		super(message);
	}

}

package com.example.quileia.exception;

public class TrackAssignationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7344854413939453696L;
	
	public TrackAssignationException() {
		super();
	}

	public TrackAssignationException(String message) {
		super(message);
	}

}

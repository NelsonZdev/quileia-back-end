package com.example.quileia.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.service.AgentService;
import com.example.quileia.service.HistoryService;
import com.example.quileia.service.TrackService;

import com.example.quileia.dto.HistoryAssignationDTO;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/history")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RestHistoryController {
	
	@Autowired
	HistoryService historyService;
	
	@Autowired
	AgentService agentService;
	
	@Autowired
	TrackService trackService;
	
	/**
	 * Muestra todos los datos de la tabla history_assignation de la base de datos.
	 * @return Retorna un arreglo que contiene todo el historial que se encuantra en la base de datos. Se retorna codigo HTTP 200
	 * En caso de no encontrar historias retorna un arreglo vacío 
	 */
	@GetMapping("/all")
	public Iterable<HistoryAssignationDTO> viewAllHistorys() {
		List<HistoryAssignationDTO> history = new ArrayList<>();
		for(HistoryAssignation h: historyService.getAllHistory()) {
			history.add(h.toConvertHistoryDTO());
		}
		return history;
	}
	
	/**
	 * Muestra el historial de un agente especifico
	 * @param id Id del agente que se desea mostrar el historial
	 * @return Retorna un arreglo con el historial del agente solicitado y retorna codigo HTTP 200
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@GetMapping("/agent-and-tracks/{agent_id}")
	public Iterable<HistoryAssignationDTO> viewAgentAndTracks(@PathVariable("agent_id") int id) throws NotFoundException {
		Agent dbAgent = agentService.getAgentById(id);
		List<HistoryAssignationDTO> history = new ArrayList<>();
		for(HistoryAssignation h: historyService.getHistoryByAgent(dbAgent)) {
			history.add(h.toConvertHistoryDTO());
		}
		return history;
	}
	
	/**
	 * Muestra el historial de una vía especifica
	 * @param id Id de la vía que se desea mostrar el historial.
	 * @return Retorna un arreglo con el historial de la vía solicitado y retorna codigo HTTP 200
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la vía en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@GetMapping("/track-and-agents/{track_id}")
	public Iterable<HistoryAssignationDTO> viewTrackAndAgents(@PathVariable("track_id") int id) throws NotFoundException{
		Track dbTrack = trackService.getTrackById(id);
		List<HistoryAssignationDTO> history = new ArrayList<>();
		for(HistoryAssignation h: historyService.getHistoryByTrack(dbTrack)) {
			history.add(h.toConvertHistoryDTO());
		}
		return history;
	}
	
}

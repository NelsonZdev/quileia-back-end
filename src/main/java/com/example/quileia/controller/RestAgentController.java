package com.example.quileia.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.quileia.dto.AgentDTO;
import com.example.quileia.entity.Agent;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.exception.TrackAssignationException;
import com.example.quileia.service.AgentService;
import com.example.quileia.service.SecretaryService;
import com.example.quileia.service.TrackService;


@RestController
@RequestMapping("agent")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RestAgentController {
	
	@Autowired
	AgentService agentService;
	
	@Autowired
	SecretaryService secretaryService;
	
	@Autowired
	TrackService trackService;
	
	/**
	 * Muestra todos los agentes de la base de datos.
	 * @return Retorna un arreglo con todos los agentes y retorna codigo HTTP 200
	 * 			En caso de no encontrar agentes retorna un arreglo vacío 
	 */
	@GetMapping("/agents")
	public Iterable<AgentDTO> getAllAgents() {
		List<AgentDTO> result = new ArrayList<>();
		for(Agent ag : agentService.getAllAgents()) {
			result.add(ag.toConvertAgentDTO());
		}
		return result;
	}
	
	/**
	 * Crea un agente en la base de datos.
	 * @param agentDTO Contiene la información del agente que se va a crear, incluído el tema de la vía.
	 * @return Una vez se ha creado el agente en base de datos, se retorna con la información del agente. 
	 *           Se retorna código HTTP 201 en caso de éxito.
	 */
	@PostMapping("/create-agent")
	@ResponseStatus(code = HttpStatus.CREATED)
	public AgentDTO postCreateAgent(@RequestBody AgentDTO agentDTO) {
		
		return agentService.createAgent(agentDTO.toConvertAgent()).toConvertAgentDTO();
		
	}
	
	/**
	 * Muestra información de un agente especifico 
	 * @param id Id del agente del que se desea obtener información.
	 * @return Si el agente se encuentra en la base de datos se retorna su información. Se genera código HTTP 200
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@GetMapping("/{id}")
	public AgentDTO getAgent(@PathVariable(name="id") int id) throws NotFoundException {

		return agentService.getAgentById(id).toConvertAgentDTO();
	}
	
	/**
	 * Permite actualizar la información de un agente.
	 * 
	 * @param agentDTO Contiene la informacción del agente que se va a editar, incluído el tema de la vía.
	 * 
	 * @return Una vez se ha editado el agente en base de datos, se retorna con la información actualizada. 
	 *         Se retorna código HTTP 202 en caso de éxito.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente que se solicita actualizar.
	 *                           Se genera HTTP 404.
	 * @throws TrackAssignationException Se lanza esta excepción en caso de que se intente asignar más de 
	 *                                   tres veces una vía al agente. Se genera HTTP 422
	 */
	@PutMapping("/edit-agent")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public AgentDTO editAgent(@RequestBody AgentDTO agentDTO) throws NotFoundException, TrackAssignationException{
		
		return agentService.updateAgent(agentDTO.toConvertAgent()).toConvertAgentDTO();
	}

	/**
	 * Permite eliminar un agente en la base de datos.
	 * @param id Id del agente del que se desea eliminar en la base de datos.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar el agente en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@DeleteMapping("/delete-agent/{id}")
	public void deleteAgent(@PathVariable(name="id") int id) throws NotFoundException {		
		agentService.deleteAgent(id);
			
	}
	
	@GetMapping("/test-service")
	public String testService() throws NotFoundException {

		return "Si Funciona";
	}
	
	@GetMapping("/test-service2")
	public String testService2() throws NotFoundException {

		return "Si Funciona N2";
	}
	
}

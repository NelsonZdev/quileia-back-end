package com.example.quileia.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.quileia.dto.SecretaryDTO;
import com.example.quileia.entity.Secretary;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.service.SecretaryService;


@RestController
@RequestMapping("secretary")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RestSecretaryController {
	
	@Autowired
	SecretaryService secretaryService;
	
	/**
	 * Muestra todos las secretarias de la base de datos.
	 * @return Retorna un arreglo con todos las secretarias y retorna codigo HTTP 200
	 * 			En caso de no encontrar secretarias retorna un arreglo vacío 
	 */
	@GetMapping("/secretarys")
	public Iterable<SecretaryDTO> getAllTracks(){
		List<SecretaryDTO> result = new ArrayList<>();
		for(Secretary sr : secretaryService.getAllSecretarys()) {
			result.add(sr.toConvertSecretaryDTO());
		}
		return result;
	}
	
	/**
	 * Muestra una secretaria especifica.
	 * @param id Id de la secretaria que se desea trer la información.
	 * @return Retorna una secretaria y retorna codigo HTTP 200
	 * @throws NotFoundException Se genera en caso de no encntrar la secretaria solicitada
	 */
	@GetMapping("/{id}")
	public SecretaryDTO getSecretaryById(@PathVariable(name = "id") int id) throws NotFoundException{
		return secretaryService.getSecretarysById(id).toConvertSecretaryDTO();
	}
	
	/**
	 * Crea una secretaria en la base de datos.
	 * @param secretaryDTO Contiene la información de la secretary que se va a crear.
	 * @return Una vez se ha creado la secretaria en base de datos, se retorna con la información de esta. 
	 *           Se retorna código HTTP 201 en caso de éxito.
	 */
	@PostMapping("/create-secretary")
	@ResponseStatus(code = HttpStatus.CREATED)
	public SecretaryDTO createSecretary(@RequestBody SecretaryDTO secretaryDTO) {			
		return secretaryService.createSecretary(secretaryDTO.toConvertSecretary()).toConvertSecretaryDTO();
	}
	
	/**
	 * Permite actualizar la informavión de una secretaria
	 * @param secretaryDTO Contiene la informacción de la secretaria que se desea editar
	 * @return Una vez se ha editado la secretaria en la base de datos, se retorna con la información actualizada. 
	 *         Se retorna código HTTP 202 en caso de éxito.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la secretaria en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@PutMapping("/edit-secretary")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public SecretaryDTO editSecretary(@RequestBody SecretaryDTO secretaryDTO) throws NotFoundException {
		return secretaryService.updateSecretary(secretaryDTO.toConvertSecretary()).toConvertSecretaryDTO();
	}
	
	/**
	 * Permite eliminar una secretaria en la base de datos.
	 * @param id Id de la secretaria que se desea eliminar en la base de datos.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la secretaria en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@DeleteMapping("/delete-secretary/{id}")
	public void deleteSecretary(@PathVariable("id") int id) throws NotFoundException {
		secretaryService.deleteSecretary(id);
	}
	
}

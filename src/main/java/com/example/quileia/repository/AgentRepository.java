package com.example.quileia.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.quileia.entity.Agent;

@Repository
public interface AgentRepository extends CrudRepository<Agent, Integer> {

}

package com.example.quileia.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.quileia.entity.Secretary;

@Repository
public interface SecretaryRepository extends CrudRepository<Secretary, Integer> {

}

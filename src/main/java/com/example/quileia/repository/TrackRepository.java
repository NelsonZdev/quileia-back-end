package com.example.quileia.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.quileia.entity.Track;

@Repository
public interface TrackRepository extends CrudRepository<Track, Integer> {

	public Iterable<Track> findAllBylevelCongestionGreaterThan(double levelCongestion);
}

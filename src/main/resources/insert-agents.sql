use Quileia;

insert into dbo.agent(last_name,name,years_experience,current_track_id,secretary_id)
values ('Moreno','Andres',12,1,2);

insert into dbo.agent(last_name,name,years_experience,current_track_id,secretary_id)
values ('Tapia','Andrea',12,1,2);

insert into dbo.agent(last_name,name,years_experience,current_track_id,secretary_id)
values ('Martinez','Daniel',12,1,2);

insert into dbo.history_assignation(agent_id, track_id,start_date,active)
values (1,1,'20120618 10:34',1)

insert into dbo.history_assignation(agent_id, track_id,start_date,active)
values (2,1,'20120618 10:34',1)

insert into dbo.history_assignation(agent_id, track_id,start_date,active)
values (3,1,'20120618 10:34',1)

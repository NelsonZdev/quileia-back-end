use Quileia

alter table dbo.agent drop constraint FKtfapdbsrdmi4gqowwd2h69ku7
alter table dbo.agent drop constraint FK88oye0n8ua8uamcl0q4f0sths
alter table dbo.history_assignation drop constraint FKt8xqfv69coq3r5ywu36vejg2u
alter table dbo.history_assignation drop constraint FK113aoutlfgl76q9ngifu4smex

truncate table dbo.agent
truncate table dbo.history_assignation
truncate table dbo.secretary
truncate table dbo.track

alter table dbo.agent add constraint FKtfapdbsrdmi4gqowwd2h69ku7 foreign key (current_track_id) references track
alter table dbo.agent add constraint FK88oye0n8ua8uamcl0q4f0sths foreign key (secretary_id) references secretary
alter table dbo.history_assignation add constraint FKt8xqfv69coq3r5ywu36vejg2u foreign key (agent_id) references agent
alter table dbo.history_assignation add constraint FK113aoutlfgl76q9ngifu4smex foreign key (track_id) references track

package com.example.quileia;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.quileia.dto.AgentDTO;
import com.example.quileia.dto.TrackDTO;
import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Secretary;
import com.example.quileia.entity.Track;

import nl.jqno.equalsverifier.EqualsVerifier;

@SpringBootTest
class EntitysTest {

	@Test
	void trackTest() {
		Track track = new Track();
		track.setId(1);
		track.setLevelCongestion(1.0);
		track.setNumber(12);
		track.setStreetOrCarrer("Carrer");
		track.setType("Street");
		
		track.getId();
		track.getLevelCongestion();
		track.getNumber();
		track.getStreetOrCarrer();
		track.getType();
		
		track.toString();
		TrackDTO trackDTO = track.toConvertTrackDTO();
		
		EqualsVerifier.simple().forClass(Track.class).withIgnoredAnnotations(Entity.class, Id.class, GeneratedValue.class,GenericGenerator.class,Column.class).verify();
		
		assertThat(trackDTO).isNotNull();
		assertThat(track).isNotNull();
	}
	
	@Test
	void trackDTOTest() {
		
		TrackDTO trackDTO = new TrackDTO(0,"Calle","Carrera",12,30);
		
		assertThat(trackDTO).isNotNull();
		
		TrackDTO track = new TrackDTO();
		track.setId(1);
		track.setLevelCongestionT(1.0);
		track.setNumberT(12);
		track.setStreetOrCarrerT("Carrer");
		track.setTypeT("Street");
		
		track.getId();
		track.getLevelCongestionT();
		track.getNumberT();
		track.getStreetOrCarrerT();
		track.getTypeT();
		
		track.toString();
		
		Track trackEntity = track.toConvertTrack();
		EqualsVerifier.simple().forClass(TrackDTO.class).verify();
		
		assertThat(trackEntity).isNotNull();
		assertThat(track).isNotNull();
	}
	
	@Test
	void historyAssignationTest() {
		HistoryAssignation history = new HistoryAssignation();
		history.setId(1);
		history.setAgent(new Agent(3));
		history.setStartDate(LocalDateTime.now());;
		history.setFinalDate(LocalDateTime.now());
		history.setTrack(new Track(1));
		history.setActive(true);
		
		history.getId();
		history.getAgent();
		history.getStartDate();;
		history.getFinalDate();
		history.getTrack();
		history.isActive();
		
		history.toString();
		
		EqualsVerifier.simple().forClass(HistoryAssignation.class).withIgnoredAnnotations(Entity.class, Id.class, GeneratedValue.class,GenericGenerator.class,Column.class).verify();
		assertThat(history).isNotNull();
	}
	
	@Test
	void secretaryTest() {
		Secretary secretary = new Secretary();
		secretary.setId(1);
		secretary.setName("test");
		
		secretary.getId();
		secretary.getName();

		
		secretary.toString();
		EqualsVerifier.simple().forClass(Secretary.class).withIgnoredAnnotations(Entity.class, Id.class, GeneratedValue.class,GenericGenerator.class,Column.class).verify();
		
		assertThat(secretary).isNotNull();
	}
	
	@Test
	void agentTest() {
		Agent agent = new Agent();
		agent.setId(1);
		agent.setName("Test");
		agent.setLastName("Test b");
		agent.setYearsExperience(12.0);
		agent.setSecretary(new Secretary(1));
		agent.setCurrentTrack(new Track(1));
		
		agent.getId();
		agent.getName();
		agent.getLastName();
		agent.getYearsExperience();
		agent.getSecretary();
		agent.getCurrentTrack();
		
		agent.toString();
		
		AgentDTO agentDTO = agent.toConvertAgentDTO();
		EqualsVerifier.simple().forClass(Agent.class).withIgnoredAnnotations(Entity.class, Id.class, GeneratedValue.class,GenericGenerator.class,Column.class).verify();
		
		assertThat(agentDTO).isNotNull();
		assertThat(agent).isNotNull();
	}
	
	@Test
	void agentDTOTest() {
		AgentDTO agent = new AgentDTO();
		agent.setId(1);
		agent.setNameA("Test");
		agent.setLastNameA("Test b");
		agent.setYearsExperienceA(12.0);
		agent.setSecretaryIdA(1);
		agent.setTrackIdA(1);
		
		agent.getId();
		agent.getNameA();
		agent.getLastNameA();
		agent.getYearsExperienceA();
		agent.getSecretaryIdA();
		agent.getTrackIdA();
		
		agent.toString();
		
		Agent agentEntity = agent.toConvertAgent();
		EqualsVerifier.simple().forClass(AgentDTO.class).verify();
		
		assertThat(agentEntity).isNotNull();
		assertThat(agent).isNotNull();
	}

}

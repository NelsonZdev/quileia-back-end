package com.example.quileia.secretary;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.quileia.service.SecretaryService;


@SpringBootTest

class SecretaryServiceTest {

	@Autowired
	SecretaryService secretaryService;
	
	@Test
	void getAllSecretarysTest() {
		assertThat(secretaryService.getAllSecretarys()).isNotNull();
	}

}

package com.example.quileia.agent;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.example.quileia.dto.AgentDTO;
import com.example.quileia.dto.HistoryAssignationDTO;
import com.example.quileia.dto.ResponseMessageDTO;
import com.example.quileia.repository.AgentRepository;
import com.example.quileia.service.AgentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.jdbc.core.JdbcTemplate;
import com.example.quileia.config.Conexion;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RestAgentControllerTest {
	
    @LocalServerPort
    private int port;
    
    private TestRestTemplate restTemplate = new TestRestTemplate();
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	AgentRepository agentRepository;
	
	@Autowired
	AgentService agentService;
	
	HttpHeaders headers = new HttpHeaders();
	
	Conexion db = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(db.connect());
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    private String toJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
    
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
    void getAllAgentsTest() throws JsonProcessingException{
    	//Agentes en script SQL
    	ArrayList<AgentDTO> agents = new ArrayList<AgentDTO>();
    	agents.add(new AgentDTO(1,"Andres","Moreno",12,1,2));
    	agents.add(new AgentDTO(2,"Andrea","Tapia",12,1,2));
    	agents.add(new AgentDTO(3,"Daniel","Martinez",12,1,2));
    	
    	String sqlCountAgents = "select count(*) from agent";
        int countAgents = this.jdbcTemplate.queryForObject(sqlCountAgents, Integer.class);
        
        assertThat(countAgents).isEqualTo(3);
    	
    	ResponseEntity<String> response = restTemplate.getForEntity(
    			createURLWithPort("/agent/agents"), String.class);
    	
    	String agentsJon = toJson(agents);

    	assertThat(response.getBody()).isEqualTo(agentsJon);
    	
    	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    	
    }
    
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
    void getAgentTest() throws JsonProcessingException{
    	//Agentes en script SQL
    	ArrayList<AgentDTO> agents = new ArrayList<AgentDTO>();
    	agents.add(new AgentDTO(1,"Andres","Moreno",12,1,2));
    	agents.add(new AgentDTO(2,"Andrea","Tapia",12,1,2));
    	agents.add(new AgentDTO(3,"Daniel","Martinez",12,1,2));
    	
    	String sqlCountAgents = "select count(*) from agent";
        int countAgents = this.jdbcTemplate.queryForObject(sqlCountAgents, Integer.class);
        
        assertThat(countAgents).isEqualTo(3);
        
        for(AgentDTO agent: agents) {
        	
        	ResponseEntity<String> response = restTemplate.getForEntity(
    			createURLWithPort("/agent/"+agent.getId()), String.class);
    	
	    	String agentJson = toJson(agent);
	    	
	    	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	    	assertThat(response.getBody()).isEqualTo(agentJson);
        }
    	
    	
    }

	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void createAgentTest() throws Exception {
		
		AgentDTO agent = new AgentDTO(0, "test", "new rest service", 7, 1, 1);
		
		HttpEntity<AgentDTO> entity = new HttpEntity<>(agent, headers);
		
        ResponseEntity<AgentDTO> response = restTemplate.postForEntity(
                createURLWithPort("/agent/create-agent"), entity, AgentDTO.class);
        
        String sqlAgent = "select * from agent where id=" + response.getBody().getId();
     
        AgentDTO dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
        new AgentDTO(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("last_name"),
                rs.getDouble("years_experience"),
                rs.getInt("current_track_id"),
                rs.getInt("secretary_id")
        ));
        
        agent.setId(response.getBody().getId());
        
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isEqualTo(agent);
        assertThat(dbAgent).isEqualTo(response.getBody());
        
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void createHistoryOfAgentTest() throws Exception {
		
		AgentDTO agent = new AgentDTO(0, "test", "new rest service", 7, 1, 1);
		
		HttpEntity<AgentDTO> entity = new HttpEntity<>(agent, headers);
        ResponseEntity<AgentDTO> response = restTemplate.postForEntity(
                createURLWithPort("/agent/create-agent"), entity, AgentDTO.class);
        
        String sqlCountAgentHistory = "select count(*) from history_assignation where agent_id="+response.getBody().getId();
    	int countAgentHistory = this.jdbcTemplate.queryForObject(sqlCountAgentHistory, Integer.class);
    	
    	assertThat(countAgentHistory).isEqualTo(1);
        
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
              
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
			"/insert-secretary-track.sql",
			"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void editAgentTest() throws Exception {
		//Agentes en script SQL
    	ArrayList<AgentDTO> agents = new ArrayList<AgentDTO>();
    	agents.add(new AgentDTO(1,"Andres","Moreno",12,1,2));
    	agents.add(new AgentDTO(2,"Andrea","Tapia",12,1,2));
    	agents.add(new AgentDTO(3,"Daniel","Martinez",12,1,2));
    	
    	String sqlCountAgents = "select count(*) from agent";
        int countAgents = this.jdbcTemplate.queryForObject(sqlCountAgents, Integer.class);
        
        assertThat(countAgents).isEqualTo(3);
    	
    	for(AgentDTO agent: agents) {
    		String sqlAgent = "select * from agent where id=" + agent.getId();
    		AgentDTO dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
            new AgentDTO(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("last_name"),
                    rs.getDouble("years_experience"),
                    rs.getInt("current_track_id"),
                    rs.getInt("secretary_id")
            ));
    		assertThat(dbAgent).isEqualTo(agent);
    	}
    	
    	for(AgentDTO agent: agents) {
    		
    		agent.setNameA("Name Edited");
    		agent.setLastNameA("Last Name Edited");
    		agent.setYearsExperienceA(5);
    		agent.setTrackIdA(2);
    		agent.setSecretaryIdA(1);
    		
    		HttpEntity<AgentDTO> entity = new HttpEntity<>(agent, headers);
    		
    		ResponseEntity<AgentDTO> response = restTemplate.exchange(createURLWithPort("/agent/edit-agent"),
            		HttpMethod.PUT, entity, AgentDTO.class);
    		
    		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    		
    		String sqlAgent = "select * from agent where id=" + response.getBody().getId();
    		AgentDTO dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
            new AgentDTO(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("last_name"),
                    rs.getDouble("years_experience"),
                    rs.getInt("current_track_id"),
                    rs.getInt("secretary_id")
            ));
    		
    		assertThat(response.getBody()).isEqualTo(agent);
    		assertThat(dbAgent).isEqualTo(agent);
    	}
    	
    	String sqlCountHistory = "select count(*) from history_assignation";
        int countHistory = this.jdbcTemplate.queryForObject(sqlCountHistory, Integer.class);
        
        assertThat(countHistory).isEqualTo(6);
		    
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
			"/insert-secretary-track.sql",
			"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void updateHistoryOfAgentTest() throws Exception {
		//Agentes en script SQL
    	ArrayList<AgentDTO> agents = new ArrayList<AgentDTO>();
    	agents.add(new AgentDTO(1,"Andres","Moreno",12,1,2));
    	agents.add(new AgentDTO(2,"Andrea","Tapia",12,1,2));
    	agents.add(new AgentDTO(3,"Daniel","Martinez",12,1,2));
    	
    	String sqlCountAgents = "select count(*) from agent";
        int countAgents = this.jdbcTemplate.queryForObject(sqlCountAgents, Integer.class);
        
        assertThat(countAgents).isEqualTo(3);
    	
    	int idhistory = 1;
    	
    	for(AgentDTO agent: agents) {
    		
    		HttpEntity<AgentDTO> entity = new HttpEntity<>(agent, headers);
    		
    		ResponseEntity<AgentDTO> response = restTemplate.exchange(createURLWithPort("/agent/edit-agent"),
            		HttpMethod.PUT, entity, AgentDTO.class);
    		
    		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    		
    		String sqlAgentHistory = "select * from history_assignation where agent_id="
					+ response.getBody().getId() + " and track_id=" + response.getBody().getTrackIdA()
					+ " and active=1"; 
    		
    		HistoryAssignationDTO history = new HistoryAssignationDTO(idhistory,agent.getId(),agent.getTrackIdA(),null,null,true);
    		
    		idhistory++;
    		
    		HistoryAssignationDTO dbAgentHistory = this.jdbcTemplate.queryForObject(sqlAgentHistory,(rs, rowNum) ->
            new HistoryAssignationDTO(
                    rs.getInt("id"),
                    rs.getInt("agent_id"),
                    rs.getInt("track_id"),
                    null,
                    null,
                    rs.getBoolean("active")
            ));
    		
    		assertThat(dbAgentHistory.toString()).hasToString(history.toString());
    	}
    	
    	String sqlCountHistory = "select count(*) from history_assignation";
        int countHistory = this.jdbcTemplate.queryForObject(sqlCountHistory, Integer.class);
        
        assertThat(countHistory).isEqualTo(3);
		    
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
			"/insert-secretary-track.sql",
			"/insert-agent-noup.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void editAgentNoTrackUpdateTest() throws Exception {
		//Agente en script SQL
		AgentDTO agent = new AgentDTO(1,"Andres","Moreno",12,1,2);
    	
    	String sqlAgent = "select * from agent where id=" + agent.getId();
    	AgentDTO dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
            new AgentDTO(
                  rs.getInt("id"),
                  rs.getString("name"),
                  rs.getString("last_name"),
                  rs.getDouble("years_experience"),
                  rs.getInt("current_track_id"),
                  rs.getInt("secretary_id")
            ));
    	
    	assertThat(dbAgent).isEqualTo(agent);
    	
    	String sqlCountAgentHistory = "select count(*) from history_assignation where agent_id="+agent.getId();
    	int countAgentHistory = this.jdbcTemplate.queryForObject(sqlCountAgentHistory, Integer.class);
    	
    	assertThat(countAgentHistory).isEqualTo(4);
    	
    	agent.setTrackIdA(1);
    	
    	HttpEntity<AgentDTO> entity = new HttpEntity<>(agent, headers);
    	
    	restTemplate.exchange(createURLWithPort("/agent/edit-agent"),
        		HttpMethod.PUT, entity, ResponseMessageDTO.class);
    	

    	//assertThat(response.getBody().getMessage()).isSubstringOf("Track no update more 3 routes assig");
    	
    	sqlAgent = "select * from agent where id=" + agent.getId();
		dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
        new AgentDTO(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("last_name"),
                rs.getDouble("years_experience"),
                rs.getInt("current_track_id"),
                rs.getInt("secretary_id")
        ));
		
		assertThat(dbAgent).isEqualTo(agent);
		
		countAgentHistory = this.jdbcTemplate.queryForObject(sqlCountAgentHistory, Integer.class);
    	
    	assertThat(countAgentHistory).isEqualTo(4);
		    
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
			"/insert-secretary-track.sql",
			"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
	void deleteAgentTest() throws Exception {
		
		ArrayList<AgentDTO> agents = new ArrayList<AgentDTO>();
    	agents.add(new AgentDTO(1,"Andres","Moreno",12,1,2));
    	agents.add(new AgentDTO(2,"Andrea","Tapia",12,1,2));
    	agents.add(new AgentDTO(3,"Daniel","Martinez",12,1,2));
    	
    	String sqlCountAgents = "select count(*) from agent";
        int countAgents = this.jdbcTemplate.queryForObject(sqlCountAgents, Integer.class);
        
        assertThat(countAgents).isEqualTo(3);
    	
    	for(AgentDTO agent: agents) {
    		String sqlAgent = "select * from agent where id=" + agent.getId();
    		AgentDTO dbAgent = this.jdbcTemplate.queryForObject(sqlAgent,(rs, rowNum) ->
            new AgentDTO(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("last_name"),
                    rs.getDouble("years_experience"),
                    rs.getInt("current_track_id"),
                    rs.getInt("secretary_id")
            ));
    		assertThat(dbAgent).isEqualTo(agent);
    	}
    	
    	
    	for(AgentDTO agent: agents) {
    		restTemplate.delete(createURLWithPort("agent/delete-agent/"+agent.getId()));
    		String sqlAgentId = "select id from agent where id=" + agent.getId();
    		assertThrows(EmptyResultDataAccessException.class, () -> this.jdbcTemplate.queryForObject(sqlAgentId, Integer.class));
    	}
        
	}
	
}

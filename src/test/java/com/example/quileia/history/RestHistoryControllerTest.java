package com.example.quileia.history;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.example.quileia.config.Conexion;
import com.example.quileia.dto.HistoryAssignationDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

class RestHistoryControllerTest {
	
	 @LocalServerPort
	 private int port;
	 
	 @Autowired
	 ObjectMapper objectMapper;
	    
	 private TestRestTemplate restTemplate = new TestRestTemplate();
	 
	 HttpHeaders headers = new HttpHeaders();
		
	Conexion db = new Conexion();
	JdbcTemplate jdbcTemplate = new JdbcTemplate(db.connect());
	
	private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    private String toJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
	void getAllHistoryTest() throws JsonProcessingException {
		//History Assignation en script SQL
		List<HistoryAssignationDTO> history = new ArrayList<>();
		history.add(new HistoryAssignationDTO(1,1,"Andres Moreno",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(2,2,"Andrea Tapia",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(3,3,"Daniel Martinez",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		
		String sqlCountHistory = "select count(*) from history_assignation";
        int countHistory = this.jdbcTemplate.queryForObject(sqlCountHistory, Integer.class);
        
        assertThat(countHistory).isEqualTo(3);
        
        ResponseEntity<String> response = restTemplate.getForEntity(
    			createURLWithPort("/history/all"), String.class);
    	
    	String historyJon = toJson(history);

    	assertThat(response.getBody()).isEqualTo(historyJon);
    	
    	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
	void getAgentAndTracksTest() throws JsonProcessingException {
		//History Assignation en script SQL
		List<HistoryAssignationDTO> history = new ArrayList<>();
		history.add(new HistoryAssignationDTO(1,1,"Andres Moreno",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(2,2,"Andrea Tapia",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(3,3,"Daniel Martinez",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		
		String sqlCountHistory = "select count(*) from history_assignation";
        int countHistory = this.jdbcTemplate.queryForObject(sqlCountHistory, Integer.class);
        
        assertThat(countHistory).isEqualTo(3);
        
//        for(HistoryAssignationDTO h: history) {
//        	String sqlHistory = "select * from history_assignation where id="+ h.getId();
//        	HistoryAssignationDTO dbHistory = this.jdbcTemplate.queryForObject(sqlHistory,(rs, rowNum) ->
//            new HistoryAssignationDTO(
//                    rs.getInt("id"),
//                    rs.getInt("agent_id"),
//                    rs.getInt("track_id"),
//                    rs.getTimestamp("start_date").toLocalDateTime(),
//                    null,
//                    rs.getBoolean("active")
//            ));
//        	assertThat(dbHistory.toString()).hasToString(h.toString());
//        }
        
        for(HistoryAssignationDTO h: history) {
        	ResponseEntity<String> response = restTemplate.getForEntity(
        			createURLWithPort("/history/agent-and-tracks/"+h.getAgentIdH()), String.class);
        	String expected = toJson(h);
        	assertThat(response.getBody()).isEqualTo("[" + expected + "]");
        }
        
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
	void getTrackAndAgentTest() throws JsonProcessingException {
		//History Assignation en script SQL
		List<HistoryAssignationDTO> history = new ArrayList<>();
		history.add(new HistoryAssignationDTO(1,1,"Andres Moreno",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(2,2,"Andrea Tapia",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		history.add(new HistoryAssignationDTO(3,3,"Daniel Martinez",1,"Calle Carrer 120",LocalDateTime.of(2012, 06, 18, 10, 34),null,true));
		
		String sqlCountHistory = "select count(*) from history_assignation";
        int countHistory = this.jdbcTemplate.queryForObject(sqlCountHistory, Integer.class);
        
        assertThat(countHistory).isEqualTo(3);
        
//        for(HistoryAssignationDTO h: history) {
//        	String sqlHistory = "select * from history_assignation where id="+ h.getId();
//        	HistoryAssignationDTO dbHistory = this.jdbcTemplate.queryForObject(sqlHistory,(rs, rowNum) ->
//            new HistoryAssignationDTO(
//                    rs.getInt("id"),
//                    rs.getInt("agent_id"),
//                    rs.getInt("track_id"),
//                    rs.getTimestamp("start_date").toLocalDateTime(),
//                    null,
//                    rs.getBoolean("active")
//            ));
//        	assertThat(h.toString()).contains(dbHistory.toString());
//        }
        
        	ResponseEntity<String> response = restTemplate.getForEntity(
        			createURLWithPort("/history/track-and-agents/"+history.get(0).getTrackIdH()), String.class);
        	String expected = toJson(history);
        	assertThat(response.getBody()).isEqualTo( expected );
        
	}

}

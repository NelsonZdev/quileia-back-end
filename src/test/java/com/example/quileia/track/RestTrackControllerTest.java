package com.example.quileia.track;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.example.quileia.dto.TrackDTO;
import com.example.quileia.repository.AgentRepository;
import com.example.quileia.service.AgentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.jdbc.core.JdbcTemplate;
import com.example.quileia.config.Conexion;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

class RestTrackControllerTest {
	
    @LocalServerPort
    private int port;
    
    TestRestTemplate restTemplate = new TestRestTemplate();
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	AgentRepository agentRepository;
	
	@Autowired
	AgentService agentService;
	
	HttpHeaders headers = new HttpHeaders();
	
	Conexion db = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(db.connect());
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    private String toJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
    
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql",
    		"/insert-secretary-track.sql",
    		"/insert-agents.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
    void getAllTracksTest() throws JsonProcessingException{
    	//Tracks en script SQL
    	ArrayList<TrackDTO> tracks = new ArrayList<TrackDTO>();
    	tracks.add(new TrackDTO(1,"Calle","Carrer",120,70));
    	tracks.add(new TrackDTO(2,"Autopista","Calle",170,70));
    	tracks.add(new TrackDTO(3,"Calle Principal","Calle",170,70));
    	
    	String sqlCountTracks = "select count(*) from track";
        int countTracks = this.jdbcTemplate.queryForObject(sqlCountTracks, Integer.class);
        
        assertThat(countTracks).isEqualTo(3);
    	
    	ResponseEntity<String> response = restTemplate.getForEntity(
    			createURLWithPort("/track/tracks"), String.class);
    	
    	String trackJon = toJson(tracks);

    	assertThat(response.getBody()).isEqualTo(trackJon);
    	
    	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    	
    }
    
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
    void getTrackTest() throws JsonProcessingException{
    	//Tracks en script SQL
    	ArrayList<TrackDTO> tracks = new ArrayList<TrackDTO>();
    	tracks.add(new TrackDTO(1,"Calle","Carrer",120,70));
    	tracks.add(new TrackDTO(2,"Autopista","Calle",170,70));
    	tracks.add(new TrackDTO(3,"Calle Principal","Calle",170,70));
    	
    	String sqlCountTracks = "select count(*) from track";
        int countTracks = this.jdbcTemplate.queryForObject(sqlCountTracks, Integer.class);
        
        assertThat(countTracks).isEqualTo(3);
        
        for(TrackDTO track: tracks) {
        	
        	ResponseEntity<String> response = restTemplate.getForEntity(
    			createURLWithPort("/track/"+track.getId()), String.class);
    	
	    	String agentJson = toJson(track);
	    	
	    	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	    	assertThat(response.getBody()).isEqualTo(agentJson);
        }
    	
    	
    }

	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql"})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void createTrackTest() throws Exception {
		
		TrackDTO track = new TrackDTO(0,"Calle","Carrer Nuevo",120,50);
		
		HttpEntity<TrackDTO> entity = new HttpEntity<>(track, headers);
		
        ResponseEntity<TrackDTO> response = restTemplate.postForEntity(
                createURLWithPort("/track/create-track"), entity, TrackDTO.class);
        
        String sqlTrack = "select * from track where id=" + response.getBody().getId();
     
        TrackDTO dbTrack = this.jdbcTemplate.queryForObject(sqlTrack,(rs, rowNum) ->
        new TrackDTO(
                rs.getInt("id"),
                rs.getString("type"),
                rs.getString("street_or_carrer"),
                rs.getInt("number"),
                rs.getDouble("level_congestion")
        ));
        
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(dbTrack).isEqualTo(response.getBody());
        
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql",})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql"})
	void editTrackTest() throws Exception {
		//Tracks en script SQL
    	ArrayList<TrackDTO> tracks = new ArrayList<TrackDTO>();
    	tracks.add(new TrackDTO(1,"Calle","Carrer",120,70));
    	tracks.add(new TrackDTO(2,"Autopista","Calle",170,70));
    	tracks.add(new TrackDTO(3,"Calle Principal","Calle",170,70));
    	
    	String sqlCountTracks = "select count(*) from track";
        int countTracks = this.jdbcTemplate.queryForObject(sqlCountTracks, Integer.class);
        
        assertThat(countTracks).isEqualTo(3);
    	
    	for(TrackDTO track: tracks) {
    		String sqlTrack = "select * from track where id=" + track.getId();
    		TrackDTO dbTrack = this.jdbcTemplate.queryForObject(sqlTrack,(rs, rowNum) ->
            new TrackDTO(
                    rs.getInt("id"),
                    rs.getString("type"),
                    rs.getString("street_or_carrer"),
                    rs.getInt("number"),
                    rs.getDouble("level_congestion")
            ));
    		assertThat(dbTrack).isEqualTo(track);
    	}
    	
    	for(TrackDTO track: tracks) {
    		
    		track.setLevelCongestionT(20);
    		track.setNumberT(100);
    		track.setStreetOrCarrerT("Edited");
    		track.setTypeT("Edited");
    		
    		HttpEntity<TrackDTO> entity = new HttpEntity<>(track, headers);
    		
    		ResponseEntity<TrackDTO> response = restTemplate.exchange(createURLWithPort("/track/edit-track"),
            		HttpMethod.PUT, entity, TrackDTO.class);
    		
    		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    		
    		String sqlTrack = "select * from track where id=" + response.getBody().getId();
    		TrackDTO dbTrack = this.jdbcTemplate.queryForObject(sqlTrack,(rs, rowNum) ->
            new TrackDTO(
                    rs.getInt("id"),
                    rs.getString("type"),
                    rs.getString("street_or_carrer"),
                    rs.getInt("number"),
                    rs.getDouble("level_congestion")
            ));
    		
    		assertThat(dbTrack).isEqualTo(track);
    	}
		    
	}
	
	@Test
	@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/truncate-db.sql","/insert-secretary-track.sql",})
	@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts =  {"/truncate-db.sql","/insert-secretary-track.sql"})
	void deleteTrackTest() throws Exception {
		
		//Tracks en script SQL
    	ArrayList<TrackDTO> tracks = new ArrayList<TrackDTO>();
    	tracks.add(new TrackDTO(1,"Calle","Carrer",120,70));
    	tracks.add(new TrackDTO(2,"Autopista","Calle",170,70));
    	tracks.add(new TrackDTO(3,"Calle Principal","Calle",170,70));
    	
    	String sqlCountTracks = "select count(*) from track";
        int countTracks = this.jdbcTemplate.queryForObject(sqlCountTracks, Integer.class);
        
        assertThat(countTracks).isEqualTo(3);
    	
        for(TrackDTO track: tracks) {
    		String sqlTrack = "select * from track where id=" + track.getId();
    		TrackDTO dbTrack = this.jdbcTemplate.queryForObject(sqlTrack,(rs, rowNum) ->
            new TrackDTO(
                    rs.getInt("id"),
                    rs.getString("type"),
                    rs.getString("street_or_carrer"),
                    rs.getInt("number"),
                    rs.getDouble("level_congestion")
            ));
    		assertThat(dbTrack).isEqualTo(track);
    	}
    	
    	for(TrackDTO track: tracks) {
    		restTemplate.delete(createURLWithPort("track/delete-track/"+track.getId()));
    		String sqlTrackId = "select id from track where id=" + track.getId();
    		assertThrows(EmptyResultDataAccessException.class, () -> this.jdbcTemplate.queryForObject(sqlTrackId, Integer.class));
    	}
        
	}
	
}

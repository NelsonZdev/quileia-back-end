package com.example.quileia;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.quileia.controller.RestAgentController;
import com.example.quileia.controller.RestHistoryController;
import com.example.quileia.controller.RestTrackController;

@SpringBootTest
class PruebaQuileiaApplicationTests {

	@Autowired
	private RestAgentController agentController;
	
	@Autowired
	private RestHistoryController historyController;
	
	@Autowired
	private RestTrackController trackController;
	
	@Test
	void contextLoads() {
		assertThat(agentController).isNotNull();
		assertThat(historyController).isNotNull();
		assertThat(trackController).isNotNull();
	}

}
